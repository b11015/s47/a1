console.log('Activity');
console.log(document);

const txtFirstName = document.querySelector("#txt-first-name");
const txtLastName = document.querySelector("#txt-last-name");
const spanFullName = document.querySelector("#span-full-name");

// 1.
// txtFirstName.addEventListener('keyup', (event) => {
//     spanFullName.innerHTML = txtFirstName.value + " " + txtLastName.value;
// });

// txtLastName.addEventListener('keyup', (event) => {

//     spanFullName.innerHTML = txtFirstName.value + " " + txtLastName.value
// });


// 2.
// function fullName() {
//     spanFullName.innerHTML = txtFirstName.value + " " + txtLastName.value;
// }

// txtFirstName.addEventListener("keyup", fullName);
// txtLastName.addEventListener("keyup", fullName);

const updateFullName = () => {
    let firstName = txtFirstName.value;
    let lastName = txtLastName.value;

    spanFullName.innerHTML = `${firstName} ${lastName}`;
    // spanFullName.innerHTML = firstName + " " + lastName;
};

txtFirstName.addEventListener("keyup", updateFullName);
txtLastName.addEventListener("keyup", updateFullName);

// stretch: manipulating key code under event properties
const keyCodeEvent = (e) => {
    let kc = e.keyCode
    if (kc === 65) {
        e.target.value = null
        alert('someone clicked a')
    }
}

txtFirstName.addEventListener('keyup', keyCodeEvent)

txtFirstName.addEventListener('keyup', (event) => {
    console.log(event);
    console.log(event.target);
    console.log(event.target.value);
});